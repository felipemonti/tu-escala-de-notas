import React from 'react'

const Footer = () =>  (
    <footer>
        <p>&copy; {new Date().getFullYear()} - Sitio desarrollado por <a className='link' href='https://beacons.ai/el.amigo.monti' target='_blank' rel='noreferrer'>"Felipe Monti"</a></p>
    </footer>
)

export default Footer;